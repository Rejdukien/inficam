#extension GL_OES_EGL_image_external : require

precision mediump float;
uniform samplerExternalOES sTexture;
varying vec2 texCoord;
uniform vec2 texSize;

uniform float sigma;
uniform float kSigma;
uniform float threshold;


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Copyright (c) 2018-2019 Michele Morrone
//  All rights reserved.
//
//  https://michelemorrone.eu - https://BrutPitt.com
//
//  me@michelemorrone.eu - brutpitt@gmail.com
//  twitter: @BrutPitt - github: BrutPitt
//
//  https://github.com/BrutPitt/glslSmartDeNoise/
//
//  This software is distributed under the terms of the BSD 2-Clause license
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#define INV_SQRT_OF_2PI 0.39894228040143267793994605993439  // 1.0/SQRT_OF_2PI
#define INV_PI 0.31830988618379067153776752674503

//  smartDeNoise - parameters
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//  sampler2D tex     - sampler image / texture
//  vec2 uv           - actual fragment coord
//  float sigma  >  0 - sigma Standard Deviation
//  float kSigma >= 0 - sigma coefficient
//      kSigma * sigma  -->  radius of the circular kernel
//  float threshold   - edge sharpening threshold
//
// vec4 smartDeNoise(sampler2D tex, vec2 uv, float sigma, float kSigma, float threshold)


// See comment by netman in fcubic.glsl... sampler2D/samplerExternalOES shenanigans.
void main(void) {
    if(sigma > 0.0) {
        float radius = kSigma*sigma;
        float radQ = radius * radius;

        float invSigmaQx2 = .5 / (sigma * sigma);      // 1.0 / (sigma^2 * 2.0)
        float invSigmaQx2PI = INV_PI * invSigmaQx2;    // 1.0 / (sqrt(PI) * sigma)

        float invThresholdSqx2 = .5 / (threshold * threshold);     // 1.0 / (sigma^2 * 2.0)
        float invThresholdSqrt2PI = INV_SQRT_OF_2PI / threshold;   // 1.0 / (sqrt(2*PI) * sigma)

        vec4 centrPx = texture2D(sTexture,texCoord);

        float zBuff = 0.0;
        vec4 aBuff = vec4(0.0);

        for(float x=-radius; x <= radius; x++) {
            float pt = sqrt(radQ-x*x);  // pt = yRadius: have circular trend
            for(float y=-pt; y <= pt; y++) {
                vec2 d = vec2(x,y);

                float blurFactor = exp( -dot(d , d) * invSigmaQx2 ) * invSigmaQx2PI;

                vec4 walkPx =  texture2D(sTexture,texCoord+d/texSize);

                vec4 dC = walkPx-centrPx;
                float deltaFactor = exp( -dot(dC, dC) * invThresholdSqx2) * invThresholdSqrt2PI * blurFactor;

                zBuff += deltaFactor;
                aBuff += deltaFactor*walkPx;
            }
        }
        gl_FragColor = aBuff/zBuff;
    } else {
        gl_FragColor = texture2D(sTexture, texCoord);
    }
}
